#!/usr/bin/env ruby

tq = 0.005
n  = 1000


start = Time.now.to_f
now   = start
prev  = start
steps = []

n.times do |n|
  sleep_thread = Thread.new { sleep(tq - 0.0008) }
  #this code shall require less than tq seconds
  now = Time.now.to_f

  # do somthing random
  rand(100).times {|i| i ** 2}
  
  steps << now - prev
  prev = now
  #now join the sleep_thread
  sleep_thread.join
end

steps.shift

avg = steps.inject(0.0) {|s,v| s + v} / steps.length
std = Math::sqrt(steps.inject(0.0) {|s,v| s + (v - avg) ** 2} / (steps.length - 1))
puts "avg:   #{avg}"
puts "error: #{avg - tq}"
puts "std:   #{std}"
puts "max:   #{steps.max}"
puts "min:   #{steps.min}"
puts "range: #{steps.max - steps.min}"









  
