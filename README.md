RNC2014
=======

The reference development system is Linux Debian 7.6.0 64 bit (amd64), even though the 32 bit should work as well. But if your PC has a 64 bit CPU, please choose the 64 bit linux version.

You can download the reference images from here:

* [Debian 7.8.0, 64 bit](http://cdimage.debian.org/debian-cd/7.8.0-live/amd64/iso-hybrid/debian-live-7.8.0-amd64-gnome-desktop.iso)
* [Debian 7.8.0, 32 bit](http://cdimage.debian.org/debian-cd/7.8.0-live/i386/iso-hybrid/debian-live-7.8.0-i386-gnome-desktop.iso)

There is a [short movie](http://vimeo.com/77040275) that shows the kind of results RNC will produce at the end of its development.

Suggested solution: Virtual Machine
-----------------------------------

It is suggested to install Debian under a virtual machine using [VirtualBox](http://www.virtualbox.org), which is a free software. Other virtualization systems (VMWare, or Parallels) can perform better, but are not free.

When creating the virtual machine select the following settings:

* Operating System: Debian 64 bit
* Disk space: >= 10 Gb
* RAM: >= 1024 Mb
* video RAM: 32 Mb, 3D acceleration
* Network: select NAT
* CD: select the virtual disk image you downloaded
* Boot order: ensure that CD comes before HD
* anything else: accept defaults

Just do a plain graphical install. **It is advised to install Debian in English language**. Be aware that for the following instructions to work, you have to be connected to the internet via a network that does not require a proxy. Your private home network and the campus wifi networks `eduroam` and `unitn-x` should work fine.

1. select English as language
2. select Other->Europe->Italy as location
3. select United States (en_US.UTF8) as locale
4. select your actual keyboard layout
5. freeley choose a host name, and use `unitn.it` as domain
6. choose a reasonably safe root password
7. freely choose the new user full name (spaces allowed)
8. choose `rnc` as username (short name, case sensitive, single word)
9. choose a reasonably safe password for the user `rnc`
10. select *Guided - use entired disk* as partitioning method, and accept defaults in the following
11. select the mirror `mirror.units.it` (under Italy) and leave blank the proxy field.
12. have a beer.

During the install, create a user with whatever long name you prefer, but with the short name `rnc`. You can actually choose different short names, but in the following instructions it will assumed that the user name is `rnc`.
Keep note of the root and user passwords you choose.

Note that VirtualBox allows you to take *snapshots* of the virtualization system: these are images of an OS at a given time, and can be used to revert to a clean state whenever something goes wrong. It is suggested that you take a snapshot immediately after the first boot, so that you can skip the installation process whenever you would need to start again from scratch.

> **NOTE**: a neat advantage of virtualization system is that the virtualized machine is actually a collection of few files that can be safely archived on an external disk, and also copied to a different host machine. For example, you could work in groups, install Debian only once and then copy the virtual machine on the laptops of the other team members, saving time.


Other solution: Dual Boot System
--------------------------------

This is the suggested way to go if you have a slow, single-core PC with less than 4 Gb of RAM. There are plenty of instructions on the Internet describing how to create a dual boot Windows/Linux install, starting from [Debian's own](http://www.debian.org/releases/stable/i386/).



Installation of prerequisites
-----------------------------

Most of the operations will be done in a terminal, or console, application. Open it from `Applications->Accessories->Terminal`.

Type the command `su`, and when prompted type the *root password*. This will give you superpowers. Now you have to also give superpowers to your standard `rnc` user. To do that, we are going to add `rnc` to a group of special administrator uses, called `sudo` (which stands for "super user do"). Type the following commands, **and double check what you typed before hitting enter!**

Also, remember that commands are always case-sensitive!

    $ usermod -aG sudo rnc

Here and in the following, the first character `$` stands for the command prompt, and you *do not* have to type it.

Now close the terminal window, click on the user name on the top right corner of the screen and select `Log out...`. This is needed for updating the settings.
Now log in again and open another Terminal.

Execute the following command for installing the needed software:

    $ wget -qO - http://bit.ly/debian_devel2014 | bash
    
This will ask for your password and take a while (~5 mins) to execute, also depending on your connection speed.

> **NOTE**: the link above has been corrected and checked. On a proper install and on a working Internet connection it works as expected.

At the end of the process, you shall see a message saying "ALL DONE!". Now type the command `ruby -v`, and if you get a message saying that
your ruby  version is `2.0.0p576` that means you are set.


Graphical viewer
----------------

The machine tool simulator that will be used for checking the proper functionality of the RNC software is included in the project under the `MTviewer` folder, for both Mac and Linux platform (**64bit only!**). 

Running on Mac
--------------

Current version also runs on Mac. On OS X, Ruby comes preinstalled and there is no need to run the setup script reported above (in fact, it won't work). Nevertheless, some Ruby gems (libraries) must be installed. 

To do that, fire up a Terminal window and enter the following commands:

    $ sudo gem install gnuplot ffi --no-rdoc --no-ri
    
Additionally, you also need Gnuplot. My suggestion is to install it via [Homebrew](http://brew.sh), but prebuilt packages available on the Internet should work as well.