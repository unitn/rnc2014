#!/usr/bin/env ruby


module RNC
  AXES = {:X => 0, :Y => 1, :Z => 2}
  COMMANDS = [:G00, :G01]
  
  class Point < Array
  
    def self.[](x=nil, y=nil, z=nil)
      self.new([x, y, z])
    end
    
    def [](index)
      super(remap_index(index))
    end
    
    def []=(index, value)
      super(remap_index(index), value)
    end
    
  
    def -(other)
      return Math::sqrt(
        (self[0] - other[0]) ** 2 +
        (self[1] - other[1]) ** 2 +
        (self[2] - other[2]) ** 2
      )
    end
    
    def delta(other)
      return Point[
        self[0] - other[0],
        self[1] - other[1],
        self[2] - other[2]
      ]
    end
    
    def modal!(other)
      [0, 1, 2].each do |i|
        self[i] = other[i] unless self[i]
      end
    end
    
    def inspect
      return "[#{self[0] or '*'}, #{self[1] or '*'}, #{self[2] or '*'}]"
    end
    
    private
    def remap_index(index)
      case index
      when Numeric
        return index.to_i
      when String
        return AXES[index.upcase.to_sym]
      when Symbol
        return AXES[index]
      else
        raise ArgumentError
      end
    end
    
  
  end # class Point
  
  
  
  
  class Block
    attr_reader :start, :target, :feed
    attr_reader :delta, :spindle_rate
    attr_reader :type, :length, :line
    attr_accessor :profile, :dt
    
    def initialize(l = "G00 X0 Y0 Z0 F1000")
      @start        = Point[nil, nil, nil]
      @target       = Point[nil, nil, nil]
      @feed         = nil
      @delta        = nil
      @spindle_rate = nil
      @type         = nil
      @length       = nil
      @profile      = nil
      @dt           = nil
      self.line     = l
    end
    
    
    def line=(l)
      @line = l.upcase
      self.parse
    end
    
    def parse
      tokens = @line.split
      @type = tokens.shift.to_sym
      raise RuntimeError, "Unsupported command #{@type}" unless COMMANDS.include? @type
      tokens.each do |t|
        cmd, arg = t[0], t[1..-1].to_f
        case cmd
        when "X", "Y", "Z"
          @target[cmd] = arg
        when "S"
          @spindle_rate = arg
        when "F"
          @feed = arg
        else
          raise RuntimeError, "Unknown G-code command #{cmd}"
        end
      end
    end
    
    def modal!(previous)
      raise ArgumentError, "I need a Block!" unless previous.kind_of? Block
      @start = previous.target
      @target.modal!(@start)
      @feed         ||= previous.feed
      @spindle_rate ||= previous.spindle_rate
      @length = @target - @start
      @delta  = @target.delta(@start)
      return self
    end
    
    def inspect
      "[#{@type} #{@target.inspect} L#{@length || '*'} F#{@feed} S#{@spindle_rate}]"
    end
  
  end # class Block



  class Parser
    attr_reader :blocks, :file_name
    def initialize(cfg)
      raise ArgumentError unless cfg.kind_of? Hash
      @cfg = cfg
      @blocks = [Block.new(cfg[:initial])]
      @file_name = cfg[:file_name]
      @profiler = Profiler.new(cfg)
    end
    
    def parse_file
      puts ">> Parsing G-code file #{@file_name}"
      File.foreach(@file_name) do |line|
        next if line.length <= 1
        puts "   parsing block #{line}"
        b = Block.new(line)
        b.modal!(@blocks.last)
        b.profile = @profiler.velocity_profile(b.feed, b.length)
        b.dt = @profiler.dt
        @blocks << b
      end
    end
    
    def each_block
      raise ArgumentError unless block_given?
      interp = Interpolator.new(@cfg)
      @blocks.each_with_index do |b, i|
        interp.block = b
        yield i, interp
      end
    end
  
  end # class Parser
  
  
  class Profiler
    attr_reader :dt, :accel, :feed, :times
    def initialize(cfg)
      @cfg   = cfg
      @feed  = 0.0
      @dt    = 0.0
      @times = []
      @accel = []
    end
    
    def velocity_profile(f_m, l)
      f_m = f_m / 60.0
      l   = l.to_f
      
      # Nominal time intervals (before quantization)
      dt_1 = f_m / @cfg[:A]
      dt_2 = f_m / @cfg[:D]
      dt_m = l / f_m - (dt_1 + dt_2) / 2.0
      
      if dt_m > 0 then # trapezoidal velocity profile
        q = quantize(dt_1 + dt_2 + dt_m, @cfg[:tq])
        dt_m += q[1]
        f_m = (2 * l) / (dt_1 + dt_2 + 2 * dt_m)
      else #triangular profile
        dt_1 = Math::sqrt(2 * l / (@cfg[:A] + @cfg[:A] ** 2 / @cfg[:D]))
        dt_2 = dt_1 * @cfg[:A] / @cfg[:D]
        q = quantize(dt_1 + dt_2, @cfg[:tq])
        dt_m = 0.0
        dt_2 += q[1] # shortcut for : dt_2 = dt_2 + q[1]
        f_m = 2 * l / (dt_1 + dt_2)
      end
      a = f_m / dt_1
      d = -(f_m / dt_2)
      
      @times = [dt_1, dt_m, dt_2]
      @accel = [a, d]
      @feed  = f_m
      @dt    = q[0]
      
      # Now calculate the integral function
      return proc do |t|
        r = 0.0
        if t < dt_1 then  # ACCELERATION
          type = :A
          r = a * t ** 2 / 2.0
        elsif t < dt_1 + dt_m then # CRUSE
          type = :M
          r = f_m * (dt_1 / 2.0 + (t - dt_1))
        else # DECELERATION
          type = :D
          t_2 = dt_1 + dt_m
          r = f_m * dt_1 / 2.0 + f_m * (dt_m + t - t_2) + d / 2.0 * (t ** 2 + t_2 ** 2) - d * t * t_2
        end
        {:lambda => r / l, :type => type}
      end #proc
    end
    
    private
    def quantize(t, dt)
      # quantize(1.0, 0.3) => [1.2, 0.2]
      result = []
      if (t % dt) == 0 then
        result = [t, 0.0]
      else
        result[0] = ((t / dt).to_i + 1) * dt
        result[1] = result[0] - t
      end
      return result
    end
  
  
  end # class Profiler
  
  
  
 
  class Interpolator
    attr_accessor :block
    def initialize(cfg)
      @cfg = cfg
      @block = nil
    end
    
    def each_timestep
      raise ArgumentError unless block_given?
      t = 0
      while (cmd = self.eval(t)) do
        yield t, cmd
        t += @cfg[:tq]
      end
    end
    
    def eval(t)
      result = {}
      case @block.type
      when :G00
        result[:position] = @block.target
        result[:lambda]   = 0.0
        result[:type]     = :R
      when :G01
        if t > @block.dt then # end of lambda function
          return nil
        end
        result = @block.profile.call(t)
        result[:position] = Point[]
        [:X, :Y, :Z].each do |axis|
          result[:position][axis] = @block.start[axis] + result[:lambda] * @block.delta[axis]
        end
      else
        raise RuntimeError, "Unimplemented command #{@block.type}"
      end
      return result
    end
  
  end # class Interpolator

end # module RNC





if __FILE__ == $0 then
  require "gnuplot"
  unless ARGV.size == 1 then
    warn "I need the name of G-code file as argument!"
    exit
  end
  
  cfg = {
    :file_name => ARGV[0],
    :A  => 5_000,
    :D  => 5_000,
    :tq => 0.01
  }

  parser = RNC::Parser.new(cfg)
  parser.parse_file
  
  n = 100
  times = []
  cmds = {:X => [], :Y => [], :Z => []}
  lambdas = []
  
  parser.each_block do |block, interp, i|
    puts "Block #{i}: #{block.inspect}"
    if block.type == :G01
      interp.each_timestep do |t, cmd|
        times << t
        [:X, :Y, :Z].each do |axis| 
          cmds[axis] << cmd[:position][axis]
        end
        lambdas << cmd[:lambda]
      end
    else
      puts "Rapid movement to #{block.target.inspect}"
    end
  end 
  
  Gnuplot.open do |gp|
    Gnuplot::Plot.new(gp) do |plot|
      plot.data = [
        Gnuplot::DataSet.new([times, lambdas]) {|ds|
          ds.with = "linespoints"
        }
      ]
    end
  end 
  
  
end


