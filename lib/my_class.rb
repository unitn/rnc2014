#!/usr/bin/env ruby

class MyClass

  attr_accessor :name
  def initialize(name)
    @name = name
  end  
end

class FileAccessor < MyClass
  
  def read_table(file=nil)
    @name = file if file.kind_of? String
    table = []                                  
    File.foreach(@name) do |line|         
      next if line[0] == "#"
      ary = line.split.map { |e| e.to_f }
      table << ary                            
    end
    return table
  end
  
  def write_table(table, header="#", file=nil)
    @name = file if file.kind_of? String
    raise "I need an array!" unless table.kind_of? Array
    @name = file if file.kind_of? String
    File.open(@name,"w") do |file|
      file.puts header
      table.each do |e|
        case e
        when Array
          file.puts e.join("\t")
        when String
          file.puts e
        else
          raise "unexpected table entry #{e}!"
        end
      end
    end
  end

end



if __FILE__ == $0 then
  puts "TESTING FileAccessor class!!!"
  
  fio = FileAccessor.new("table.txt")
  t = [[1, 2], [3, 4], [5, 6]]
  fio.write_table(t, "x\ty")
end
