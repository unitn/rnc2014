#!/usr/bin/env ruby

require "gnuplot"
require "./lib/parser.rb"
require "./lib/machine2.rb"
require "./MTviewer/viewer.rb"


# Check for command line arguments
unless ARGV.size == 1 then
  warn "I need the name of G-code file as argument!"
  exit
end
  
# Set configuration variables
INITIAL_POS = RNC::Point[0,0,370]

CFG = {
  :file_name => ARGV[0],
  :A  => 728,
  :D  => 728,
  :tq => 0.005,
  :initial => "G00 X#{INITIAL_POS[:X]} Y#{INITIAL_POS[:Y]} Z#{INITIAL_POS[:Z]} F1000",
  :tq_corr => 0.0008
}

MAX_ERROR = 0.005

# Machine tool simulator
m = RNC::Machine.new
m.load_configs %w(lib/X.yaml lib/Y.yaml lib/Z.yaml)
m.go_to INITIAL_POS
m.reset

# Machine viewer
case RUBY_PLATFORM
when /darwin/
  viewer = Viewer::Link.new("./MTviewer/mac/MTviewer")
when /linux/
  viewer = Viewer::Link.new("./MTviewer/linux/MTviewer")
else
  raise "Unsupporte platform for MTviewer"
end
viewer.go_to INITIAL_POS

# Instantiate main objects
parser = RNC::Parser.new(CFG)

# Main code
parser.parse_file

puts "=" * 79
puts "Press SPACE in the viewer window to start"
puts "=" * 79
loop until (viewer.run)

data = {
  time:[], block_time:[], n_block:[], lambda:[],
  Xn:[], Yn:[], Zn:[],
  X:[], Y:[], Z:[]
}
format = "%7.3f %7.3f %3d %6.4f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f"
 
File.open("profiles.txt", "w") do |data_file|
  data_file.puts "# #{data.keys * ' '}"
  now = 0.0
  start_time = Time.now
  parser.each_block do |n_block, interp|
    puts "Block #{n_block}: #{interp.block.inspect}"
    case interp.block.type
    when :G00 #Rapid positioning
      m.go_to(interp.block.target.map {|e| e / 1000.0}) # from mm to m
      t = 0.0
      begin
        sleep_thread = Thread.new { sleep CFG[:tq] - CFG[:tq_corr]}
        
        now = Time.now - start_time
        state = m.step!
        state[:pos].map! {|e| e * 1000.0} # from m to mm
        error = m.error * 1000.0          # from m to mm
        
        data[:time]       << now
        data[:block_time] << t
        data[:n_block]    << n_block
        data[:lambda]     << 0.0

        {Xn:0, Yn:1, Zn:2}.each {|axis,idx| data[axis] << state[:pos][idx]}
        [:X, :Y, :Z].each {|axis| data[axis] << interp.block.target[axis]}
        
        data_file.puts format % (data.values.map {|v| v.last})
        
        viewer.go_to state[:pos]
        
        t += CFG[:tq]
        sleep_thread.join
      end while (error >= MAX_ERROR)
    
    when :G01 # Linear interpolation
      interp.each_timestep do |t, cmd|
        sleep_thread = Thread.new { sleep CFG[:tq] - CFG[:tq_corr] } 
        
        now = Time.now - start_time
        m.go_to(cmd[:position].map {|e| e / 1000.0}) # from mm to m
        state = m.step!
        state[:pos].map! {|e| e * 1000.0} # from m to mm
        
        data[:time]       << now
        data[:block_time] << t
        data[:n_block]    << n_block
        data[:lambda]     << cmd[:lambda]

        {Xn:0, Yn:1, Zn:2}.each {|axis,idx| data[axis] << state[:pos][idx]}
        [:X, :Y, :Z].each {|axis| data[axis] << cmd[:position][axis]}
        
        data_file.puts format % (data.values.map {|v| v.last})
        
        viewer.go_to state[:pos]
        sleep_thread.join
      end
    else
      warn "Skipping unsupported block #{block.line}"
    end
  end 
end # closing the data_file


puts "=" * 79
puts "Press SPACE in the viewer window to stop"
puts "=" * 79
loop while (viewer.run)
viewer.close



Gnuplot.open do |gp|
  Gnuplot::SPlot.new(gp) do |plot|
    plot.data = [
      Gnuplot::DataSet.new([data[:X], data[:Y], data[:Z]]) {|ds|
        ds.with = "lines"
        ds.title = "Nominal"
      },
      Gnuplot::DataSet.new([data[:Xn], data[:Yn], data[:Zn]]) {|ds|
        ds.with = "lines"
        ds.title = "Actual"
      }
    ]
  end
end 
  
  
