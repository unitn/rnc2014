clear all
close all
clc
addpath(genpath('./YAML'));

%% Axis parameters (to be customized)
% axis parameters
path = '../lib/'; % where to put generated yaml files
axes(1).name = 'X';
axes(1).Isat = 70.0;
axes(1).m    = 500.0;
axes(1).kfr  = 0.0;

axes(2).name = 'Y';
axes(2).Isat = 70.0;
axes(2).m    = 250.0;
axes(2).kfr  = 0.0;

axes(3).name = 'Z';
axes(3).Isat = 70.0;
axes(3).m    = 50.0;
axes(3).kfr  = 0.0;

%% Loop over axes

for i = 1:1%length(axes)
  axis_name = axes(i).name;
  Isat = axes(i).Isat; % Saturation current
  m    = axes(i).m;
  kfr  = axes(i).kfr;

  Ktr  = 0.0016;
  Kd   = 0.15;

  % sampling time for the discretization
  Ts = 5e-3;


  % system parameters
  Kt =.3;
  Sg = 0.0648;
  KI = 25.5;
  Kv = 21.934;
  La = 2e-3;
  Je = 0.0036;%Je=0.0036;%(original value, motor only)
  B  = 0.096;%B=0.09;%(original value, motor only)
  Ra = 0.4;
  Ka = 0.0643;
  Kb = 0.3;
  Hg = 0.08872;
  Tg = 0.13183;

  Kl = 0.8085e-3;
  % Ke=636.62;
  % Kd=0.00488;


  K1 = Kt*Sg*KI*Kv/La/Je;
  K2 = B/Je+(Ra+Kv*Ka)/La;
  K3 = (B*(Ra+Kv*Ka)+Kt*(Kb+Hg*Tg*Kv*KI))/(Je*La);

  s = tf('s');

  % plant
  G = K1/(s^2+K2*s+K3)*Kl/s;

  % P parameters
  wcp = 40;

  K_P = wcp*K3/K1/Kl;

  R_P = K_P;

  Lp = R_P*G;

  % figure
  % margin(Lp)
  % grid on
  % 
  % figure
  % step(Lp/(1+Lp))
  % grid on
  % 
  % figure
  % pzmap(Lp,minreal(Lp/(1+Lp)))

  % discretization
  % continuous time space state matrices
  % states=[Ia w Xa].'
  % inputs=[Vc Tr 0].'
  % outputs=all states
  Act=[-(Kv*Ka+Ra)/La     -(Kb+Kv*KI*Tg*Hg)/La            -Kv*KI*Sg*K_P/La;
       Kt/(Je+Ktr*Kl*m)   -(B+Ktr*Kd*Kl)/(Je+Ktr*Kl*m)    0;
       0                  Kl                              0];
  Bct=[Kv*KI*Sg/La*K_P 0                0;
       0               -1/(Je+Ktr*Kl*m) 0;
       0               0                0];
  Cct=eye(3);
  Dct=zeros(3,3);

  % discrete time space state matrices
  % not checking if A singular (it would mean very bad controller design)

  Adt = expm(Act*Ts);
  Bdt = Act\(Adt-eye(3))*Bct;
  Cdt = Cct;
  Ddt = Dct;

  aux_data = struct('mass',m,'ts',Ts,'static_fr',kfr,'saturation',Isat);
  generate_rb_data(axis_name,path,Adt,Bdt,aux_data);
end

