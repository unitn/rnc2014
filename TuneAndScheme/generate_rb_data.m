function [filename] = generate_rb_data( axis, path, A, B, params)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
  data = struct('axis',axis,'mat',struct('Adt', A, 'Bdt', B),'params',params);
  filename = strcat(path,axis,'.yaml');
  result = WriteYaml(filename,data);
  fprintf('wrote file %s for axis %s\n', filename, axis);
end

