#!/usr/bin/env ruby

# Collections (Array):
an_array = [1, 2, 3.5, "test", ["a", "b"]]
puts an_array[10]
an_array[1] = "replaced element"
an_array[10] = 10
puts
p an_array
puts an_array.inspect
puts an_array.class


# Collections (Hash):

my_hash = { "a" => 1, "b" => 2, :c => 4 }
puts my_hash["a"] 

hsh = {a:1, b:2, c:3}  # corresponds to {:a => 1, :b => 2, :c =>3}
p hsh 

hsh  = {}
hsh[:d] = 4
hsh[:e] = 5

p hsh

puts "*" * 70
puts "Defining classes"



class Person
  attr_accessor :surname # creates setter and getter for surname
  
  def initialize(name)
    @name = name
    @surname = nil
  end
  
  def greet
    if @name == @surname then
      puts "Hello, I'm #{@name} #{@surname} (strange, though!)"
    elsif @surname then
      puts "Hello, I'm #{@name} #{@surname}!"
    else
      puts "Hello, I'm #{@name}!"
    end
  end # greet
  
  # "getter" accessor method
  def name
    return @name
  end
  
  # "setter" accessor method
  def name=(v)
    @name = v
  end
  
end # Person

me = Person.new("Paolo")
me.greet

you = Person.new("Hugo")
you.greet

p me

# using a getter metod:
puts "name of me is #{me.name}"

# using a setter method:
me.name = "Pietro"
p me
me.name = "Paolo"
me.greet
me.surname = "Bosetti"
me.greet

me.surname = "Paolo"
me.greet


class Male < Person

  def gender
    return "male"
  end
  
  def greet
    super # this calls the greet method as defined in the superclass
    puts "and I am a #{self.gender}"
  end
  
end # Male

class Female < Male

  def gender; "female"; end

end # Female

me  = Male.new("Paolo")
me.surname = "Bosetti"
puts me.gender
puts me.greet


puts "*" * 70
puts "Iterations"

10.times do |i|
  puts "#{i}: using times"
end

5.upto(15) do |i|
  puts "#{i}: using upto"
end

countdown = ["three", "two", "one"].reverse
(countdown.size - 1).downto(0) do |i|
  puts "#{countdown[i]}..."
end
puts "zero!"

puts "\nUsing each:"
countdown = ["three", "two", "one"]
countdown.each do |e|
  puts "#{e}..."
end
puts "zero!"



























