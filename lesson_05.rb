#!/usr/bin/env ruby

require "gnuplot"

x = (0..50).map {|v| v.to_f}
y = x.map {|v| v ** 2 }
z = x.map {|v| v ** 3 }
    

Gnuplot.open do |gp|
  Gnuplot::Plot.new(gp) do |plot|
    plot.title "Gnuplot from Ruby"
    plot.xlabel "x"
    plot.ylabel "y"
    
    plot.data = [
      Gnuplot::DataSet.new([x, y]) { |ds|
        ds.with = "linespoints"
        ds.title = "x^2"
      },
      Gnuplot::DataSet.new([x, z]) { |ds|
        ds.with = "lines"
        ds.title = "x^3"
      }
    ]
  end
end
