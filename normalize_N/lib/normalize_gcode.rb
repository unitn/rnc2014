#!/usr/bin/env ruby

class NormalizeGCode
  attr_accessor :filename
  def initialize(filename)
    @lines = []
    @lines_out = []
    self.filename = filename
  end
  
  def filename=(n)
    @filename = n
    self.parse
  end
  
  def parse(base = 0, increment = 10)
    i = base
    @lines_in = []
    @lines_out = []
    File.foreach(@filename) do |line|
      tokens = line.upcase.split
      tokens.shift if tokens[0][0] == "N"
      tokens.unshift("N#{i}")
      @lines_out << tokens.join(' ')
      i += increment
    end
  end
  
  def output(line_end = "\n")
    return @lines_out.join(line_end)
  end
  
  def write(filename)
    raise RuntimeError, "File #{filename} exists!" if File.exist? filename
    File.open(filename, "w") { |file| file.puts self.output }
  end
  
end

