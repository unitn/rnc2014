#!/usr/bin/env ruby


# Proc objects creation:
my_proc = proc {|x, y| x ** y}

# Usage (call)
puts my_proc.call(2, 4)



