#!/usr/bin/env ruby

# each method and iterators

a = [1,2,3]

a.each_with_index do |e, i|
  puts "a[#{i}] = #{e}"
end

a.map! { |e| e.to_f }

p a

sum = a.inject(0) {|s, e| s + e }
puts sum

prod = a.inject {|accum, element| accum * element}
puts  prod

desc = a.inject("") {|accum, element| accum + element.to_s + " "}
puts desc

puts a * ", "

puts 

# Hash iterators

h  = {a:1, b:2, c:3}

h.each do |key, value|
  puts "h[:#{key}] = #{value}"
end

p h.keys
p h.values


# How to define functions/method using blocks

def repeat(what, n=10)
  if block_given?
    n.times do |i|
      yield(what, i)
    end
  else
    puts what
  end
end

repeat("Hello")































