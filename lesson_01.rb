#!/usr/bin/env ruby
# line above is called "shebang"



def hello(name)
  # String interpolation
  puts("Hello, #{name}!") # replaces the current value of name within the string
  puts "2+2 = #{2+2}"
end

hello("Paolo")


# variables:
a_string = "this is a string value"
puts a_string

# constants
PI = 3.141592654352
puts PI
PI = 3 # you get a warning here!
puts PI

# Basic types
a = 1   # this is a Fixnum
b = 2.2 # this is a Float

puts "a is a #{a.class}"
puts "b is a #{b.class}"
puts "a_string is a #{a_string.class}"
puts

# Collections:
an_array = [1, 2, 3.5, "test"]
puts an_array[10]
an_array[1] = "replaced element"
an_array[10] = 10
puts
p an_array
puts an_array.inspect

puts an_array.class






