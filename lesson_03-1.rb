#!/usr/bin/env ruby

require "./lib/my_class"

mc = MyClass.new("example")
p mc

file_name = "test.txt"
File.open(file_name, "w") do  |file|
  file.puts "# x y"
  10.times do |i|
    file.puts "#{i.to_f} #{i.to_f ** 2}"
  end
end

puts "Content of #{file_name}"
table = []
File.foreach(file_name) do |line|
  next if line[0] == "#"
  table << line.split.map {|e| e.to_f }
end

p table


